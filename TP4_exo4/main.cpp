#include <bits/types/time_t.h>
#include <time.h>
#include "glm/ext/scalar_constants.hpp"
#include "glm/fwd.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "p6/p6.h"

struct Vertex2DUV {
    glm::vec2 position;
    glm::vec2 texture;

    Vertex2DUV(glm::vec2 pos, glm::vec2 text)
    {
        position = pos;
        texture  = text;
    }
};

glm::mat3 translate(float tx, float ty)
{
    glm::mat3 M = glm::mat3(glm::vec3(1, 0, 0), glm::vec3(0, 1, 0), glm::vec3(tx, ty, 1));
    return M;
}

glm::mat3 scale(float sx, float sy)
{
    return glm::mat3(
        glm::vec3(sx, 0, 0),
        glm::vec3(0, sy, 0),
        glm::vec3(0, 0, 1)
    );
}

glm::mat3 rotate(float a)
{
    float angle = glm::radians(a);
    return glm::mat3(
        glm::vec3(glm::cos(angle), glm::sin(angle), 0),
        glm::vec3(-glm::sin(angle), glm::cos(angle), 0),
        glm::vec3(0, 0, 1)
    );
}

int main()
{
    auto ctx = p6::Context{{1280, 720, "TP4 EX3"}};
    ctx.maximize_window();

    const p6::Shader shader = p6::load_shader(
        "shaders/tex2D.vs.glsl",
        "shaders/tex2D.fs.glsl"
    );

    GLint U_MATRIX_MODEL_LOCATION = glGetUniformLocation(shader.id(), "uModelMatrix");
    GLint U_COLOR                 = glGetUniformLocation(shader.id(), "uColor");

    // creation du vbo
    GLuint vbo;
    glGenBuffers(1, &vbo);
    // binding du vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    // remplissage VBO
    Vertex2DUV vertices[] = {
        Vertex2DUV{{-1.f, -1.f}, {0, 0}},
        Vertex2DUV{{1.f, -1.f}, {0, 0}},
        Vertex2DUV{{0.f, 1.f}, {0, 0}},

    };

    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(Vertex2DUV), vertices, GL_STATIC_DRAW);
    // debinder le VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // création du VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    // binder le VAO
    glBindVertexArray(vao);
    // activation des attributs de vertex
    static constexpr GLuint VERTEX_ATTR_POSITION = 0;
    static constexpr GLuint VERTEX_ATTR_TEXTURE  = 1;
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);
    //  spécification des attributs de vertex
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DUV), (const GLvoid*)offsetof(Vertex2DUV, position));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DUV), (const GLvoid*)offsetof(Vertex2DUV, texture));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    // Declare your infinite update loop.

    time_t begin;
    time_t end;
    begin = time(&begin);

    ctx.update = [&]() {
        end            = time(&end);
        time_t elapsed = end - begin;
        // glm::mat3 rotation = rotate(static_cast<float>(elapsed));
        shader.use();
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);
        glBindVertexArray(vao);

        glm::vec3 Color0 = glm::vec3(1, 1, 1);
        glm::vec3 Color1 = glm::vec3(0, 0, 1);
        glm::vec3 Color2 = glm::vec3(1, 0, 0);
        glm::vec3 Color3 = glm::vec3(0, 1, 0);

        glm::mat3 MRotateGlobal = rotate(elapsed * 50);
        glm::mat3 MRotate       = rotate(elapsed * 100);
        glm::mat3 MRotateInv    = rotate(-elapsed * 100);
        glm::mat3 MScale        = scale(0.5, 0.5);

        glm::mat3 MTranslate0 = translate(1, 10);
        glUniform3fv(U_COLOR, 1, glm::value_ptr(Color0));
        glUniformMatrix3fv(U_MATRIX_MODEL_LOCATION, 1, GL_FALSE, glm::value_ptr(MRotateGlobal * MScale * MTranslate0 * MRotate));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glm::mat3 MTranslate1 = translate(1, -1);
        glUniform3fv(U_COLOR, 1, glm::value_ptr(Color1));
        glUniformMatrix3fv(U_MATRIX_MODEL_LOCATION, 1, GL_FALSE, glm::value_ptr(MRotateGlobal * MScale * MTranslate1 * MRotateInv));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glm::mat3 MTranslate2 = translate(-1, 1);
        glUniform3fv(U_COLOR, 1, glm::value_ptr(Color2));
        glUniformMatrix3fv(U_MATRIX_MODEL_LOCATION, 1, GL_FALSE, glm::value_ptr(MRotateGlobal * MScale * MTranslate2 * MRotateInv));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glm::mat3 MTranslate3 = translate(-1, -1);
        glUniform3fv(U_COLOR, 1, glm::value_ptr(Color3));
        glUniformMatrix3fv(U_MATRIX_MODEL_LOCATION, 1, GL_FALSE, glm::value_ptr(MRotateGlobal * MScale * MTranslate3 * MRotate));
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glBindVertexArray(0);
        /*********************************GLsizei stride
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
    };

    // Should be done last. It starts the infinite loop.
    ctx.start();

    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}