#version 330 core

layout(location = 0) in vec2 aVertexPosition;
layout (location = 1) in vec2 aVertexTexture;

uniform mat3 uModelMatrix;
uniform vec3 uColor;

out vec3 vFragColor;

void main()
{
    gl_Position = vec4((vec3(aVertexPosition, 1.)*uModelMatrix).xy, 0., 1.);
    vFragColor = uColor;
}

