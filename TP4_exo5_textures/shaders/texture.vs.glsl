#version 330 core

layout(location = 0) in vec2 aVertexPosition;
layout (location = 1) in vec2 aTexturePosition;

uniform mat3 uModelMatrix;
uniform vec3 uColor;

out vec2 vTextCoord;

void main()
{
    gl_Position = vec4(aVertexPosition, 0., 1.);
    vTextCoord = aTexturePosition;
}
