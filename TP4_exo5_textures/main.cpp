#include <bits/types/time_t.h>
#include <time.h>
#include "glimac/default_shader.hpp"
#include "glm/ext/scalar_constants.hpp"
#include "glm/fwd.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "p6/p6.h"

struct Vertex2DUV {
    glm::vec2 position;
    glm::vec2 texture;

    Vertex2DUV(glm::vec2 pos, glm::vec2 text)
    {
        position = pos;
        texture  = text;
    }
};

glm::mat3 translate(float tx, float ty)
{
    glm::mat3 M = glm::mat3(glm::vec3(1, 0, 0), glm::vec3(0, 1, 0), glm::vec3(tx, ty, 1));
    return M;
}

glm::mat3 scale(float sx, float sy)
{
    return glm::mat3(
        glm::vec3(sx, 0, 0),
        glm::vec3(0, sy, 0),
        glm::vec3(0, 0, 1)
    );
}

glm::mat3 rotate(float a)
{
    float angle = glm::radians(a);
    return glm::mat3(
        glm::vec3(glm::cos(angle), glm::sin(angle), 0),
        glm::vec3(-glm::sin(angle), glm::cos(angle), 0),
        glm::vec3(0, 0, 1)
    );
}

int main()
{
    auto ctx = p6::Context{{1280, 720, "TP4 EX3"}};
    ctx.maximize_window();

    const p6::Shader shader = p6::load_shader(
        "shaders/texture.vs.glsl",
        "shaders/texture.fs.glsl"
    );

    shader.use();

    // GLint U_MATRIX_MODEL_LOCATION = glGetUniformLocation(shader.id(), "uModelMatrix");
    // GLint U_COLOR                 = glGetUniformLocation(shader.id(), "uColor");
    GLint U_TEXTURE = glGetUniformLocation(shader.id(), "utexture");

    // creation du vbo
    GLuint vbo;
    glGenBuffers(1, &vbo);

    // creatuion de la texture
    img::Image texture = p6::load_image_buffer(
        "assets/textures/triforce.png"
    );
    GLuint textures;
    glGenTextures(1, &textures);

    // binding du vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    // binding de la texture
    glBindTexture(GL_TEXTURE_2D, textures);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture.width(), texture.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // debindage de la texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // remplissage VBO
    Vertex2DUV vertices[] = {
        Vertex2DUV{{-0.5f, -0.5f}, {0.f, 0.f}},
        Vertex2DUV{{0.5f, -0.5f}, {1.f, 0.f}},
        Vertex2DUV{{0.f, 0.5f}, {0.5f, 1.f}},

    };

    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(Vertex2DUV), vertices, GL_STATIC_DRAW);
    // debinder le VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // création du VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);
    // binder le VAO
    glBindVertexArray(vao);
    // activation des attributs de vertex
    static constexpr GLuint VERTEX_ATTR_POSITION = 0;
    static constexpr GLuint VERTEX_ATTR_TEXTURE  = 1;
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);
    //  spécification des attributs de vertex
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DUV), (const GLvoid*)offsetof(Vertex2DUV, position));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DUV), (const GLvoid*)offsetof(Vertex2DUV, texture));

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Declare your infinite update loop.

    ctx.update = [&]() {
        shader.use();
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);
        glBindVertexArray(vao);

        glBindTexture(GL_TEXTURE_2D, textures);
        glUniform1i(U_TEXTURE, 0);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(0);
        /*********************************GLsizei stride
         * HERE SHOULD COME THE RENDERING CODE
         *********************************/
    };

    // Should be done last. It starts the infinite loop.
    ctx.start();

    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &textures);
}