#include <cstddef>
#include <vector>
#include "glimac/common.hpp"
#include "glimac/sphere_vertices.hpp"
#include "glm/ext/quaternion_trigonometric.hpp"
#include "glm/ext/scalar_constants.hpp"
#include "glm/fwd.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/trigonometric.hpp"
#include "p6/p6.h"

int main()
{
    auto ctx = p6::Context{{1280, 720, "TP6 EX1"}};
    ctx.maximize_window();

    struct EarthProgram {
        p6::Shader m_Program;

        GLint uMVPMatrix;
        GLint uMVMatrix;
        GLint uNormalMatrix;
        GLint uEarthTexture;
        GLint uCloudsTexture;

        EarthProgram()
            : m_Program{p6::load_shader("shaders/3D.vs.glsl", "shaders/multitext3D.fs.glsl")}
        {
            uMVPMatrix     = glGetUniformLocation(m_Program.id(), "uMVPMatrix");
            uMVMatrix      = glGetUniformLocation(m_Program.id(), "uMVMatrix");
            uNormalMatrix  = glGetUniformLocation(m_Program.id(), "uNormalMatrix");
            uEarthTexture  = glGetUniformLocation(m_Program.id(), "uEarthTexture");
            uCloudsTexture = glGetUniformLocation(m_Program.id(), "uCloudsTexture");
        }
    };

    struct MoonProgram {
        p6::Shader m_Program;

        GLint uMVPMatrix;
        GLint uMVMatrix;
        GLint uNormalMatrix;
        GLint uTexture;

        MoonProgram()
            : m_Program{p6::load_shader("shaders/3D.vs.glsl", "shaders/tex3D.fs.glsl")}
        {
            uMVPMatrix    = glGetUniformLocation(m_Program.id(), "uMVPMatrix");
            uMVMatrix     = glGetUniformLocation(m_Program.id(), "uMVMatrix");
            uNormalMatrix = glGetUniformLocation(m_Program.id(), "uNormalMatrix");
            uTexture      = glGetUniformLocation(m_Program.id(), "uTexture");
        }
    };

    EarthProgram earthProgram{};
    MoonProgram  moonProgram{};

    glEnable(GL_DEPTH_TEST);

    // creation du vbo
    GLuint vbo;
    glGenBuffers(1, &vbo);

    // chargement des images
    img::Image texture_earth = p6::load_image_buffer(
        "assets/textures/EarthMap.jpg"
    );

    img::Image texture_clouds = p6::load_image_buffer(
        "assets/textures/CloudMap.jpg"
    );

    img::Image texture_moon = p6::load_image_buffer(
        "assets/textures/MoonMap.jpg"
    );

    GLuint textureEarth, textureClouds, textureMoon;

    // binding du vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glGenTextures(1, &textureEarth);
    glBindTexture(GL_TEXTURE_2D, textureEarth); // Binding de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_earth.width(), texture_earth.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_earth.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &textureMoon);
    glBindTexture(GL_TEXTURE_2D, textureMoon); // Binding de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_moon.width(), texture_moon.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_moon.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    glGenTextures(1, &textureClouds);
    glBindTexture(GL_TEXTURE_2D, textureClouds); // Binding de la texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_clouds.width(), texture_clouds.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_clouds.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);

    // remplissage VBO
    const std::vector<glimac::ShapeVertex> vertices = glimac::sphere_vertices(1.f, 32, 16);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glimac::ShapeVertex), vertices.data(), GL_STATIC_DRAW);

    // debinder le VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // création du VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);

    // binder le VAO
    glBindVertexArray(vao);

    // activation des attributs de vertex
    static constexpr GLuint VERTEX_ATTR_POSITION = 0;
    static constexpr GLuint VERTEX_NORMAL        = 1;
    static constexpr GLuint TEXT_COORD           = 2;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_NORMAL);
    glEnableVertexAttribArray(TEXT_COORD);
    // spécification des attributs de vertex

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, position));
    glVertexAttribPointer(VERTEX_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, normal));
    glVertexAttribPointer(TEXT_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, texCoords));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Declare your infinite update loop.

    std::vector<glm::vec3> rotateAxes;
    std::vector<glm::vec3> translations;

    for (int i = 0; i < 32; i++)
    {
        rotateAxes.push_back(glm::sphericalRand(1.0f));
        translations.push_back(glm::sphericalRand(2.0f));
    }

    ctx.update = [&]() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindVertexArray(vao);

        glm::mat4 projMatrix = glm::perspective(glm::radians(70.f), 800.f / 600.f, 0.1f, 100.f);

        ////// TERRE //////

        earthProgram.m_Program.use();

        glUniform1i(earthProgram.uEarthTexture, 0);
        glUniform1i(earthProgram.uCloudsTexture, 1);

        const glm::mat4 globalMVMatrix = glm::translate(glm::mat4{1.f}, {0.f, 0.f, -5.f});

        const glm::mat4 earthMVMatrix = glm::rotate(globalMVMatrix, ctx.time(), {0.f, 1.f, 0.f});
        glUniformMatrix4fv(earthProgram.uMVMatrix, 1, GL_FALSE, glm::value_ptr(earthMVMatrix));
        glUniformMatrix4fv(earthProgram.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(glm::transpose(glm::inverse(earthMVMatrix))));
        glUniformMatrix4fv(earthProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix * earthMVMatrix));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureEarth);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureClouds);

        glDrawArrays(GL_TRIANGLES, 0, vertices.size());

        ////// LUNES //////

        // on charge la texture de la Lune dans la "case 0"
        glBindTexture(GL_TEXTURE_2D, textureMoon);

        // on les dessine

        for (size_t i = 0; i < rotateAxes.size(); i++)
        {
            glm::mat4 MVMatrix2     = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5));
            glm::mat4 NormalMatrix2 = glm::transpose(glm::inverse(MVMatrix2));
            MVMatrix2               = glm::rotate(MVMatrix2, ctx.time(), rotateAxes.at(i)); // Translation * Rotation
            MVMatrix2               = glm::translate(MVMatrix2, translations.at(i));        // Translation * Rotation * Translation
            MVMatrix2               = glm::scale(MVMatrix2, glm::vec3(0.2, 0.2, 0.2));      // Translation * Rotation * Translation * Scale
            glUniformMatrix4fv(moonProgram.uMVPMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix * MVMatrix2));
            glUniformMatrix4fv(moonProgram.uMVMatrix, 1, GL_FALSE, glm::value_ptr(MVMatrix2));
            glUniformMatrix4fv(moonProgram.uNormalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix2));
            glDrawArrays(GL_TRIANGLES, 0, vertices.size());
        }

        // debinder le vbo et toutes les textures
        glBindVertexArray(0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
    };

    // Should be done last. It starts the infinite loop.
    ctx.start();

    glDeleteTextures(1, &textureClouds);
    glDeleteTextures(1, &textureEarth);
    glDeleteTextures(1, &textureMoon);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}