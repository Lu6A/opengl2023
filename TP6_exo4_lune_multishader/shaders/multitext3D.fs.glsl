#version 330 core

in vec2 vTextCoord;
out vec4 fFragColor;

uniform sampler2D uTexture1;
uniform sampler2D uTexture2;

void main()
{
    vec4 color = texture(uTexture1, vTextCoord) + texture(uTexture2, vTextCoord);
    fFragColor = vec4(color.xyz, 1.f);
}

