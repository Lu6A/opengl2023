#include <cstddef>
#include <vector>
#include "glimac/common.hpp"
#include "glimac/sphere_vertices.hpp"
#include "glm/ext/quaternion_trigonometric.hpp"
#include "glm/ext/scalar_constants.hpp"
#include "glm/fwd.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/random.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/trigonometric.hpp"
#include "p6/p6.h"

int main()
{
    auto ctx = p6::Context{{1280, 720, "TP6 EX1"}};
    ctx.maximize_window();

    const p6::Shader shader = p6::load_shader(
        "shaders/3D.vs.glsl",
        "shaders/tex3D.fs.glsl"
    );

    shader.use();

    GLuint U_MVP_MATRIX_LOCATION    = glGetUniformLocation(shader.id(), "uMVPMatrix");
    GLuint U_MV_MATRIX_LOCATION     = glGetUniformLocation(shader.id(), "uMVMatrix");
    GLuint U_NORMAL_MATRIX_LOCATION = glGetUniformLocation(shader.id(), "uNormalMatrix");
    GLint  U_TEXTURE                = glGetUniformLocation(shader.id(), "utexture");

    glEnable(GL_DEPTH_TEST);

    // creation du vbo
    GLuint vbo;
    glGenBuffers(1, &vbo);

    // cration de la texture
    img::Image texture_earth = p6::load_image_buffer(
        "assets/textures/EarthMap.jpg"
    );

    img::Image texture_moon = p6::load_image_buffer(
        "assets/textures/MoonMap.jpg"
    );

    GLuint textures;
    glGenTextures(1, &textures);

    // binding du vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    // binding de la texture
    glBindTexture(GL_TEXTURE_2D, textures);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // debindage de la texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // remplissage VBO
    const std::vector<glimac::ShapeVertex> vertices = glimac::sphere_vertices(1.f, 32, 16);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glimac::ShapeVertex), vertices.data(), GL_STATIC_DRAW);

    // debinder le VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // création du VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);

    // binder le VAO
    glBindVertexArray(vao);

    // activation des attributs de vertex
    static constexpr GLuint VERTEX_ATTR_POSITION = 0;
    static constexpr GLuint VERTEX_NORMAL        = 1;
    static constexpr GLuint TEXT_COORD           = 2;

    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_NORMAL);
    glEnableVertexAttribArray(TEXT_COORD);
    // spécification des attributs de vertex

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, position));
    glVertexAttribPointer(VERTEX_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, normal));
    glVertexAttribPointer(TEXT_COORD, 2, GL_FLOAT, GL_FALSE, sizeof(glimac::ShapeVertex), (const GLvoid*)offsetof(glimac::ShapeVertex, texCoords));
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Declare your infinite update loop.

    std::vector<glm::vec3> rotateAxes;
    std::vector<glm::vec3> translations;

    for (int i = 0; i < 32; i++)
    {
        rotateAxes.push_back(glm::sphericalRand(1.0f));
        translations.push_back(glm::sphericalRand(2.0f));
    }

    ctx.update = [&]() {
        shader.use();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindVertexArray(vao);

        // terre
        glBindTexture(GL_TEXTURE_2D, textures);
        glUniform1i(U_TEXTURE, 0);

        // on charge la texture de la Terre
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_earth.width(), texture_earth.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_earth.data());

        // on la dessine
        glm::mat4 ProjMatrix   = glm::perspective(glm::radians(70.f), 800.f / 600.f, 0.1f, 100.f);
        glm::mat4 MVMatrix     = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5));
        glm::mat4 NormalMatrix = glm::transpose(glm::inverse(MVMatrix));
        glUniformMatrix4fv(U_MVP_MATRIX_LOCATION, 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix));
        glUniformMatrix4fv(U_MV_MATRIX_LOCATION, 1, GL_FALSE, glm::value_ptr(MVMatrix));
        glUniformMatrix4fv(U_NORMAL_MATRIX_LOCATION, 1, GL_FALSE, glm::value_ptr(NormalMatrix));
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());

        // on charge la texture de la lune
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture_moon.width(), texture_moon.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_moon.data());

        // on les dessine

        for (size_t i = 0; i < rotateAxes.size(); i++)
        {
            glm::mat4 MVMatrix2     = glm::translate(glm::mat4(1), glm::vec3(0, 0, -5));
            glm::mat4 NormalMatrix2 = glm::transpose(glm::inverse(MVMatrix2));
            MVMatrix2               = glm::rotate(MVMatrix2, ctx.time(), rotateAxes.at(i)); // Translation * Rotation
            MVMatrix2               = glm::translate(MVMatrix2, translations.at(i));        // Translation * Rotation * Translation
            MVMatrix2               = glm::scale(MVMatrix2, glm::vec3(0.2, 0.2, 0.2));      // Translation * Rotation * Translation * Scale
            glUniformMatrix4fv(U_MVP_MATRIX_LOCATION, 1, GL_FALSE, glm::value_ptr(ProjMatrix * MVMatrix2));
            glUniformMatrix4fv(U_MV_MATRIX_LOCATION, 1, GL_FALSE, glm::value_ptr(MVMatrix2));
            glUniformMatrix4fv(U_NORMAL_MATRIX_LOCATION, 1, GL_FALSE, glm::value_ptr(NormalMatrix2));
            glDrawArrays(GL_TRIANGLES, 0, vertices.size());
        }

        // debinder le vbo
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(0);
    };

    // Should be done last. It starts the infinite loop.
    ctx.start();

    glDeleteTextures(1, &textures);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}