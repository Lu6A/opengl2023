#version 330 core

in vec3 vVertexPositionVS;
in vec3 vVertexNormalVS;
in vec2 vTextCoord;

out vec3 fFragColor;

void main() {
    fFragColor = vec3(normalize(vVertexNormalVS));
}