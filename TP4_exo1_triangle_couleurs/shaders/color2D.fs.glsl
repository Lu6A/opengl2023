#version 330 core

in vec3 vFragColor;
in vec2 vVertexPosition ;

out vec4 fFragColor;


float attenuation(float alpha, float beta)
{
  return alpha*exp(-beta*distance(vVertexPosition,vec2(0,0))*distance(vVertexPosition,vec2(0,0)));
}


void main() {
  float a = attenuation(2.,70.);
  fFragColor = vec4(a*vFragColor.x, a*vFragColor.y, a*vFragColor.z, 1.);
};