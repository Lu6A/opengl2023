#version 330 core

layout(location = 0) in vec2 aVertexPosition;
layout(location = 1) in vec3 aVertexColor;

out vec3 vFragColor;
out vec2 vVertexPosition;

mat3 translate(float tx, float ty){
return mat3(
    vec3(1, 0, 0),
    vec3(0, 1, 0),
    vec3(tx, ty, 1)
);
}


mat3 scale(float sx, float sy){
return mat3(
    vec3(sx, 0, 0),
    vec3(0, sy, 0),
    vec3(0, 0, 1)
);
}

mat3 rotate(float a){
  float angle = radians(a); 
return mat3(
    vec3(cos(angle), sin(angle), 0),
    vec3(-sin(angle), cos(angle), 0),
    vec3(0, 0, 1)
);
}



vec2 transformed = (vec3(aVertexPosition, 1.)* translate(0.5,0)*rotate(45)*scale(0.5, 0.5)).xy ;


void main() { 
  vFragColor = aVertexColor;
  vVertexPosition = aVertexPosition;
  gl_Position = vec4((vec3(aVertexPosition,1.)).xy, 0., 1.);
};