#version 330 core

in vec2 vTextCoord;
out vec4 fFragColor;

uniform sampler2D uTexture;

void main()
{
    vec4 color = texture(uTexture, vTextCoord);
    fFragColor = vec4(color.xyz, 1.f);
}

