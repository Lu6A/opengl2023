#version 330 core

in vec2 vTextCoord;
out vec4 fFragColor;

uniform sampler2D uEarthTexture;
uniform sampler2D uCloudsTexture;

void main()
{
    vec4 color = texture(uEarthTexture, vTextCoord) + texture(uCloudsTexture, vTextCoord);
    fFragColor = vec4(color.xyz, 1.f);
}

